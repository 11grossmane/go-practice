package module01

import "math"

func GCD(a, b int) int {
	var larger int
	var greatest float64
	if a > b {
		larger = a
	} else {
		larger = b
	}
	for i := 1; i <= larger/2; i++ {
		if a%i == 0 && b%i == 0 {
			greatest = math.Max(float64(greatest), float64(i))
		}
	}
	return int(greatest)
}
