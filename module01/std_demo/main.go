package main

import (
	"algo/module01"
	"fmt"
)

func main() {

	module01.FizzBuzz(20)
	fmt.Println(module01.FindTwoThatSum([]int{1, 2, 3, 4}, 7))
	fmt.Println(module01.Factor([]int{2, 3, 5}, 720))
	fmt.Println(module01.Fibonacci(20))
	fmt.Println(module01.GCD(8500, 9800))
	// var n int
	// fmt.Scanf("%d", &n)
	// for i := 0; i < n; i++ {
	// 	var a, b int
	// 	fmt.Scanf("%d %d", &a, &b)
	// 	gcd := module01.GCD(a, b)
	// 	fmt.Println(gcd)

	// }
}
