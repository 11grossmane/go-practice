package module01

// Fibonacci returns the nth fibonacci number.
//
// A Fibonacci number N is defined as:
//
//   Fibonacci(N) = Fibonacci(N-1) + Fibonacci(N-2)
//
// Where the following base cases are used:
//
//   Fibonacci(0) => 0
//   Fibonacci(1) => 1
//
//
// Examples:
//
//   Fibonacci(0) => 0
//   Fibonacci(1) => 1
//   Fibonacci(2) => 1
//   Fibonacci(3) => 2
//   Fibonacci(4) => 3
//   Fibonacci(5) => 5
//   Fibonacci(6) => 8
//   Fibonacci(7) => 13
//   Fibonacci(14) => 377
//
import (
	"log"
	"time"
)

func runningtime(s string) (string, time.Time) {
	log.Println("Start:	", s)
	return s, time.Now()
}

func track(s string, startTime time.Time) {
	endTime := time.Now()
	log.Println("End:	", s, "took", endTime.Sub(startTime))
}

func Fibonacci(n int) int {
	defer track(runningtime("Fibonnaci"))
	cache := make(map[int]int)
	return helper(n, cache)
}

func helper(h int, cache map[int]int) int {
	if h <= 1 {
		return h
	}
	sum := 0
	_, ok := cache[h]
	if ok == true {
		return cache[h]
	} else {
		sum = helper(h-1, cache) + helper(h-2, cache)
		cache[h] = sum
	}
	return sum
}
